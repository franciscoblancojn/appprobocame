
/**
 * @description init object function (f)
 */
 var f = {}

/**
 * respond
 * @description hace respuesta al endpoint
 * @param {*} res 
 * @param {*} responde 
 */
 f.respond = (res , responde) => {
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(responde));
    res.flush()
    res.end();
}

/**
 * base64
 * @description convierte a base64
 * @param e 
 * @returns string 
 */
 f.base64 = (e) => {
    return Buffer.from(e).toString('base64')
}


module.exports = f;
