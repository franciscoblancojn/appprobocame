const nonce = require('nonce')();
const express = require('express');
const dotenv = require('dotenv');
const fs = require('fs');

//npm install nonce express dotenv fs

dotenv.config();
  
const http = express();
const port = 3001;

http.get('/app', async (req, res) => {
    const {shop , session } = req.query;
    if(session){
        fs.readFile(__dirname + '/app.html', function (err, html) {
            if (err) {
                throw err; 
            }    
            res.send(`
                <script>
                    appJson = ${JSON.stringify(req.query)};
                </script>
                ${html}
            `)   
        });
    }else if (shop) {

        const state = nonce();
        // shopify callback redirect
        const redirectURL = process.env.SHOPIFY_APP_URL + '/auth/callback';

        // Install URL for app install
        const shopifyURL = 'https://' + shop +
            '/admin/oauth/request_grant?client_id=' + process.env.SHOPIFY_API_KEY +
            '&scope=' + process.env.SHOPIFY_API_SCOPES +
            '&redirect_uri=' + redirectURL +
            '&state=' + state ;

        res.cookie('state', state);
        res.redirect(shopifyURL);
    } else {
        return res.status(400).send('Missing "Shop Name" parameter!!');
    }
});

http.listen(port, () => console.log('Application listening on port 3434!'));
