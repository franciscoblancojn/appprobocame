const crypto = require('crypto');
const request = require('request-promise');
const querystring = require('querystring');
const cookie = require('cookie');
const express = require('express');
const dotenv = require('dotenv');

//npm install nonce express dotenv fs crypto request-promise querystring cookie

const dbMongo = require("../dbMongo")


dotenv.config();

const saveToken = (host, token) => {
    dbMongo.loadConfig(process.env.mongoCheckVerify,null)

    dbMongo.addTokenShopify(
        {
            mongoCheckVerify:process.env.mongoCheckVerify,
            host,
            token
        },
        (e)=>{}
    )
}


const http = express();
const port = 3002;

http.get('/auth/callback', (req, res) => {
    const {shop, hmac, code, state} = req.query;
    const stateCookie = cookie.parse(req.headers.cookie).state;

    if (shop && hmac && code) {
        const queryMap = Object.assign({}, req.query);
        delete queryMap['signature'];
        delete queryMap['hmac'];

        const message = querystring.stringify(queryMap);
        const providedHmac = Buffer.from(hmac, 'utf-8');
        const generatedHash = Buffer.from(crypto.createHmac('sha256', process.env.SHOPIFY_API_SECRET_KEY).update(message).digest('hex'), 'utf-8');

        let hashEquals = false;

        try {
            hashEquals = crypto.timingSafeEqual(generatedHash, providedHmac);
        } catch (e) {
            hashEquals = false;
        }

        if (!hashEquals) {
            return res.status(400).send('HMAC validation failed');
        }
        const accessTokenRequestUrl = 'https://' + shop + '/admin/oauth/access_token';
        const accessTokenPayload = {
            client_id: process.env.SHOPIFY_API_KEY,
            client_secret: process.env.SHOPIFY_API_SECRET_KEY,
            code,
        };

        request.post(accessTokenRequestUrl, {json: accessTokenPayload})
            .then((accessTokenResponse) => {
                const accessToken = accessTokenResponse.access_token;
                const shopRequestURL = 'https://' + shop + '/admin/api/2020-04/shop.json';
                const shopRequestHeaders = {'X-Shopify-Access-Token': accessToken};
                saveToken(shop,accessToken)
                request.get(shopRequestURL, {headers: shopRequestHeaders})
                    .then((shopResponse) => {
                        res.redirect('https://' + shop + '/admin/apps/'+process.env.NAMEAPP);
                    })
                    .catch((error) => {
                        res.status(error.statusCode).send(error.error.error_description);
                    });
            })
            .catch((error) => {
                res.status(error.statusCode).send(error.error.error_description);
            });

    } else {
        res.status(400).send('Required parameters missing');
    }
});

http.listen(port, () => console.log('Application listening on port 3434!'));
