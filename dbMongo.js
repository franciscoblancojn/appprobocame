const dbMonk = require('monk')('localhost/Elios');
const f = require("./functions");


var db = {}
/**
 * loadConfig
 * @description Funcion para configurar json y res
 * @state produccion
 * @param {json,res}
 */
db.loadConfig = (mongoCheckVerify,res) => {
    db.mongoCheckVerify = mongoCheckVerify
    db.res = res
}

/**
 * addTokenShopify
 * @description Funcion agregar Token para Shopify
 * @state produccion
 * @param {json,respond()}
 * @return {json} 
 */
 db.addTokenShopify = (json,respond = (e) => db.respond(e)) => {
    if(!db.validator(json.mongoCheckVerify)){
        respond({
            type:"error",
            msj:"Error the authentication"
        })
        return;
    }
    if(db.mongoCheckVerify!=json.mongoCheckVerify){
        respond({
            type:"error",
            msj:"Error the authentication"
        })
        return;
    }
    if(!db.validator(json.host)){
        respond({
            type:"error",
            msj:"Error host"
        })
        return;
    }
    if(!db.validator(json.token)){
        respond({
            type:"error",
            msj:"Error token"
        })
        return;
    }
    var token = dbMonk.get('TokenShopify')
    token.update(
        {
            host:json.host
        },
        {
            $set: {
                host : json.host,
                token : json.token
            }
        },
        {
            "upsert":true
        }
    ).then(function (e) {
        respond(e)
    })
}

/**
 * getTokenShopify
 * @description Funcion obtener Token para Shopify
 * @state produccion
 * @param {json,respond()}
 * @return {json} 
 */
 db.getTokenShopify = (json,respond = (e) => db.respond(e)) => {
    if(!db.validator(json.mongoCheckVerify)){
        respond({
            type:"error",
            msj:"Error the authentication"
        })
        return;
    }
    if(db.mongoCheckVerify!=json.mongoCheckVerify){
        respond({
            type:"error",
            msj:"Error the authentication"
        })
        return;
    }
    
    var token = dbMonk.get('TokenShopify')
    token.find(json.query || {},json.return || {}).then(function (e) {
        respond(e)
    })
}

module.exports = db;
